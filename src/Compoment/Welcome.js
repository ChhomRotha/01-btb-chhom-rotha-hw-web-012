import React from 'react'
import { Container } from 'react-bootstrap'

function Welcome() {
    return (
        <Container>
            <h1>Welcome</h1>
        </Container>
    )
}

export default Welcome
