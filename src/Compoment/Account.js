import React from 'react'
import {Link, Route, Switch} from 'react-router-dom';
import queryString from 'query-string';

function Account(props) {
    // console.log(props.location.search);
     let pv = queryString.parse(props.location.search);
     console.log(pv);
    //console.log(props.location.search);
    return (
        <div className = "container">
            <h1>Accounts</h1>
            <ul>
                <li>
                    <Link to = {`/Account?name=Netfix`}>Netfix</Link>
                </li>
                <li>
                    <Link to = {`?name=ZillowGroup`}>Zillow Group</Link>
                </li>
                <li>
                    <Link to = {`?name=Yahoo`}>Yahoo</Link>
                </li>
                <li>
                    <Link to = {`?name=ModusCreate`}>Modus Create</Link>
                </li>
            </ul>
            <Switch>
               <Route path = {`/Account`}>
                    <h3>This name in the query string is"{pv.name}"</h3>
               </Route>

            </Switch>
        </div>
    )
}

export default Account
