import React, { Component } from 'react'
import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default class Menu extends Component {
    render() {
        return (
            <div>   
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand as = {Link} to = "/">React Router</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link as = {Link} to = "/Home">Home</Nav.Link>
                                <Nav.Link as = {Link} to = "/Video">Video</Nav.Link>
                                {/* <Nav.Link as = {Link} to = "/Account">Account</Nav.Link> */}
                                <Nav.Link as = {Link} to = "/Account">Account</Nav.Link>
                                <Nav.Link as = {Link} to = "/Auth">Auth</Nav.Link>
                            </Nav>
                            <Nav>
                                <Nav.Link href="#deets">More deets</Nav.Link>
                                <Nav.Link eventKey={2} href="#memes">
                                    Dank memes
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                    </Navbar>
            </div>
        )
    }
}
